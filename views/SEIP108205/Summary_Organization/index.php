<?php

include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."AtomicProject_Mostakim_108205".DIRECTORY_SEPARATOR."vendor/autoload.php");
        
use \App\BITM\SEIP108205\Summary_Organization\Summary_Organization;

$summary=new Summary_Organization();
$texts=$summary->index();


?>
     

<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body >
        <div align="center";>
        <h1>Summary Text</h1>
        <div><span>Search / Filter </span> 
            <span >Download as PDF | XL  <a href="./create.php">create New</a></span>
            <select>
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
        </div>
        <table border="1">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>Summary Text</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach($texts as $summary):
                ?>
                <tr>
                    <td><?php echo $summary['id'];?></td>
                    <td><?php echo $summary['text'];?></td>
                    <td>View | Edit | Delete | Trash/Recover | Email to Friend</td>
                </tr>
                <?php
                 endforeach;
                ?>
 
            </tbody>
        </table>
        <div><span> prev  1 | 2 | 3 next </span></div>
        <a href="https://localhost/AtomicProject_Mostakim_108205/index.php">Back use absolute</a>
        <a href="../index.php">Back using relative path</a>
    </div>
    </body>
</html>
